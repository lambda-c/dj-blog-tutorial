import os
import sys
from logging import Filter, getLogger
from logging.config import dictConfig
from pathlib import Path


PWD = Path(__file__).resolve()
LOG_DIR = PWD.parent.parent.joinpath("logs")
LOG_FILE = LOG_DIR.joinpath("django.log")

# LOG_DIRが存在しない場合、新規作成
if not LOG_DIR.exists():
    LOG_DIR.mkdir()

time  = "%(asctime)s.%(msecs)03d"
lvl   = "[%(levelname)s]"
mod   = "[%(module)s:%(lineno)d]"
msg   = "%(message)s"

formatters = {
    "default": {
        "datefmt": "%Y-%m-%d_%I-%M-%S",
        "format": "\t".join([time, mod, lvl, msg])
    },
}

handlers = {
    "file": {
        "level": "DEBUG",
        "class": "logging.handlers.TimedRotatingFileHandler",
        "filename": LOG_FILE,
        "when": "D",
        "encoding": "utf-8",
        "formatter": "default",
    },
    "console": {
        "level": "DEBUG",
        "class": "logging.StreamHandler",
        "formatter": "default",
    },
}

loggers = {
    "blog": {
        "handlers": ["file"],
        "level": "WARNING",
        "propagate": False,
    },
}

root = {
    "handlers": ["file"],
    "level": "WARNING",
}


def get_logging_conf(is_debug):
    if is_debug:
        # デバッグ時はログレベルを下げ、SQLログも残す
        loggers["django.db.backends"] = {
            "handlers": ["file"],
            "level": "DEBUG",
            "propagate": False,
        }
        loggers["blog"]["handlers"] = ["file", "console"]
        loggers["blog"]["level"] = "DEBUG"
        root["handlers"] = ["file"]
        root["level"] = "DEBUG"

        if "runserver" in sys.argv:
            # デバッグ時かつ開発用サーバ起動時のみコンソールにもログ表示
            loggers["django.db.backends"]["handlers"] = ["file", "console"]
            root["handlers"] = ["file", "console"]

    return dict(
        version = 1,
        disable_existing_loggers = False,
        formatters = formatters,
        handlers = handlers,
        loggers = loggers,
        root = root
    )
