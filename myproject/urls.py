from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.contrib.sitemaps.views import sitemap
from django.contrib.sitemaps import GenericSitemap
from django.views.generic import TemplateView
from django.views.decorators.cache import cache_page

from blog import views
from blog.models import Article


handler400 = views.ArticleListView.as_view(status_code=400)
handler403 = views.ArticleListView.as_view(status_code=403)
handler404 = views.ArticleListView.as_view(status_code=404)

info_dict = {
    "queryset": Article.objects.filter(is_public=True).order_by("-created_at"),
    "date_field": "updated_at",
}

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", include("blog.urls")),
    path('markdownx/', include('markdownx.urls')),
    # the sitemap
    path('sitemap.xml', sitemap,
         {'sitemaps': {'blog': GenericSitemap(info_dict, priority=0.5, changefreq="weekly",)}},
         name='django.contrib.sitemaps.views.sitemap'),
    # the robots.txt
    path('robots.txt',
         cache_page(60*60*24*7)(TemplateView.as_view(template_name="robots.txt", content_type='text/plain')),
         name="robots.txt"),
]


if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [path('__debug__/', include(debug_toolbar.urls))]

from django.conf.urls.static import static
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
