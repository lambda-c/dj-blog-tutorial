from .models import Article


def custom_variable(request):
    return dict(
        populars = Article.objects.filter(is_public=True).order_by("-counter")[:5],
        latests = Article.objects.filter(is_public=True).order_by("-created_at")[:5]
    )
