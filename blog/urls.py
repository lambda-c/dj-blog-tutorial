from django.urls import path, include

from .views import ArticleListView, ArticleDetailView, ContactCreateView


app_name = "blog"

urlpatterns = [
    path("articles/", ArticleListView.as_view(), name="list"),
    path("articles/<str:title>", ArticleDetailView.as_view(), name="detail"),
    path('contact/', ContactCreateView.as_view(), name='contact'),
]
