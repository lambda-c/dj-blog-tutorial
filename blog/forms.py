from django import forms

from .models import Comment


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = (
            "name",
            "description",
            "parent",
        )
        widgets = {
            "parent": forms.HiddenInput(),
        }

    def clean(self):
        cleaned_data = super().clean()
        parent = cleaned_data.get("parent", None)
        if parent and parent.depth >= 4:
            raise forms.ValidationError("これ以上、返信できません")

        return cleaned_data
