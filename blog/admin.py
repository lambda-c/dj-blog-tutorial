from io import StringIO

from django.contrib import admin
from django.core.management import call_command
from django.http import HttpResponseRedirect
from django.urls import path

from markdownx.admin import MarkdownxModelAdmin

from .models import Article, Tag, Comment, Contact


@admin.register(Article)
class AdminArticle(MarkdownxModelAdmin):
    """
    """
    ### カスタムコマンドの実行
    # カスタムコマンド用のテンプレート
    change_list_template = "blog/command.html"

    # カスタムコマンドの実行
    def cacheclear(self, request):
        with StringIO() as out:
            call_command("clearcache", stdout=out)
        self.message_user(request, "cleared cache")
        return HttpResponseRedirect("../")

    # カスタムコマンドに割り振るURLを追加
    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('command/cacheclear', self.cacheclear),
        ]
        return urls + my_urls

    ### 一覧画面用に関係する項目
    # 記事取得のクエリー
    # tagを表示するため、ページ表示時に予めtagsを取得しておく
    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related("tags")

    # tagの表示内容
    def tag(self, obj):
        return ", ".join(tag.name for tag in obj.tags.all())

    # 検索対象
    search_fields = (
        'title',
        'content',
    )

    # フィルタリング要素
    list_filter = (
        "is_public",
    )

    # 記事の表示個数
    list_per_page = 50

    # 一覧画面、編集画面での表示項目
    list_display = (
        'title',
        'id',
        'created_at',
        'updated_at',
        'is_public',
        'counter',
        'tag',  # モデルに存在しない項目、上記で表示する内容を同メソッドで定義
    )

    ### 以下は記事編集画面に関係する項目
    # 編集画面での画面区分け
    fieldsets = (
        # 区分け名、対応するフィールド群
        ('basic', {'fields': ('title', 'content',)}),
        ('tags', {'fields': ('tags',)}),
        ('meta', {'fields': ('is_public', 'counter',)}),
    )

    # 編集画面での操作
    filter_horizontal = (
        "tags",
    )


@admin.register(Tag)
class AdminTag(admin.ModelAdmin):
    """
    """


@admin.register(Comment)
class AdminComment(admin.ModelAdmin):
    """
    """


@admin.register(Contact)
class AdminContact(admin.ModelAdmin):
    """
    """
