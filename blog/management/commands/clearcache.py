from django.core.management.base import BaseCommand
from django.core.cache import cache


class Command(BaseCommand):
    help = 'clear cache'

    def handle(self, *args, **kwargs):
        try:
            cache.clear()
            self.stdout.write(
                self.style.SUCCESS(
                    "cleared cache"
                )
            )
        except Exception as e:
            self.stdout.write(
                self.style.WARNING(
                    "failed clearing cache, because %s" % e
                )
            )
