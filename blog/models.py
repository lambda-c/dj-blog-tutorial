from django.db import models
from django.utils.functional import cached_property
from django.core.cache import cache

from markdownx.models import MarkdownxField
from markdownx.utils import markdownify


class Tag(models.Model):
    name = models.CharField(max_length=30)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Article(models.Model):
    title = models.CharField(max_length=255, unique=True)
    content = MarkdownxField()
    tags = models.ManyToManyField(Tag, blank=True)
    is_public = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    counter = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return "/articles/"+self.title

    @cached_property
    def get_next_article(self):
        try:
            return self.get_next_by_created_at(is_public=True)
        except self.DoesNotExist as e:
            return None

    @cached_property
    def get_prev_article(self):
        try:
            return self.get_previous_by_created_at(is_public=True)
        except self.DoesNotExist as e:
            return None

    @property
    def formatted_markdown(self):
        return markdownify(self.content)

    def countup(self):
        key = "count"+str(self.id)
        unit = 10
        cache.get_or_set(key, unit, timeout=60*60*24*30)
        cache.decr(key)
        if cache.get(key) <= 0:
            self.counter += unit
            self.save(update_fields=["counter"])
            cache.incr(key, unit)
        return self.counter


class Comment(models.Model):
    name = models.CharField(max_length=30)
    article = models.ForeignKey(Article,
        related_name='comments', on_delete=models.SET_NULL, null=True)
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    parent = models.ForeignKey('self', blank=True, null=True, related_name='children', on_delete=models.CASCADE)
    depth = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.name


class Contact(models.Model):
    name = models.CharField(max_length=30)
    subject = models.CharField(max_length=255, help_text="お問い合わせタイトルをご入力ください")
    mail = models.EmailField(help_text="返信先のメールアドレスをご入力ください")
    description = models.TextField(max_length=2048)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name
