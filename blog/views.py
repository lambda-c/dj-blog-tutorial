from logging import getLogger

from django.views.generic import CreateView, ListView, DetailView, TemplateView
from django.views.generic.edit import FormMixin
from django.urls import reverse
from django.contrib import messages
from django.urls import reverse_lazy
from django.conf import settings
from django.core.mail import send_mail
from django.db.models import Q

from .models import Article, Tag, Comment, Contact
from .forms import CommentForm


logger = getLogger(__name__)

err_msg = {
    400: "リクエストに不整合がありました",
    403: "そのページは見ることができません",
    404: "ページが見つかりませんでした",
}


class ArticleListView(ListView):
    """
    ブログ記事一覧のビューを提供する
    """
    template_name = "blog/list.html"
    context_object_name = "articles"
    model = Article
    paginate_by = 10
    http_method_names = ['get']
    status_code = None
    errors = (400, 403, 404)

    def get_queryset(self):
        q = self.request.GET.get("q", "")
        qs = Article.objects.prefetch_related("tags").filter(is_public=True).order_by("-created_at")

        if 2 < len(q):
            searched_qs = qs.filter(Q(title__contains=q) | \
                                    Q(content__contains=q) | \
                                    Q(tags__name__icontains=q))\
                            .distinct()\
                            .order_by('-created_at')
            if 0 < len(searched_qs):
                return searched_qs
            else:
                messages.error(self.request, q+"の検索結果は0件でした")
        return qs

    def get(self, request, *args, **kwargs):
        if self.status_code in self.errors:
            request.GET = request.GET.copy()
            request.GET["page"] = 1
            request.GET["q"] = ""
        res = super().get(request, *args, **kwargs)
        if self.status_code in self.errors:
            res.status_code = self.status_code
            messages.error(request, err_msg[self.status_code])
        return res

    def get_context_data(self, **kwargs):
        if self.request.GET.get("q", ""):
            kwargs["q"] = "q={}".format(self.request.GET.get("q"))
        return super().get_context_data(**kwargs)


class ArticleDetailView(FormMixin, DetailView):
    """
    ブログ記事のビューを提供する
    ただしブログ記事にはコメント投稿が備わっているため、
    `CreateView`ではなく`FormMixin`, `DetailView`を使用する
    """
    template_name = "blog/detail.html"
    context_object_name = "article"
    model = Article
    form_class = CommentForm
    slug_url_kwarg = "title"
    slug_field = "title"

    def get_queryset(self):
        return Article.objects.filter(is_public=True)

    def get_success_url(self):
        return reverse("blog:detail", kwargs={"title": self.object})

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        comment = form.save(commit=False)
        comment.article = self.object
        if comment.parent:
            comment.article = comment.parent.article
            comment.depth = comment.parent.depth + 1
        comment.save()
        return super().form_valid(form)

    def get(self, request, *args, **kwargs):
        res = super().get(request, *args, **kwargs)
        self.object.countup()
        return res

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["similars"] = Article.objects\
                                     .filter(tags__in=self.object.tags.all())\
                                     .exclude(id=self.object.id)\
                                     .distinct()\
                                     .order_by('?')[:3]
        context["comments"] = self.convert_comment()
        return context

    def convert_comment(self):
        """
        DBへの繰り返しアクセスを避けるため、
        コメントレコードを入れ子構造のJSONに変換する
        """
        comments = self.object.comments.order_by("-created_at").values()
        converted = []

        for comment in comments:
            comment["children"] = []

        def get_parent(_id):
            for comment in comments:
                if comment["id"] is _id:
                    return comment
            raise Article.DoesNotExist("can't find article id={}".format(_id))

        for comment in comments:
            if comment["depth"] is not 0:
                parent = get_parent(_id=comment["parent_id"])
                parent["children"].insert(0, comment)
            else:
                converted.insert(0, comment)

        return converted


class ContactCreateView(CreateView):
    template_name = "blog/contact.html"
    model = Contact
    fields = ("name", "subject", "mail", "description")
    success_url = reverse_lazy('blog:contact')

    def form_valid(self, form):
        responce = super().form_valid(form)
        subject = form.cleaned_data['subject']
        from_email = form.cleaned_data['mail']
        message = form.cleaned_data['description']
        to_email = settings.EMAIL_HOST
        try:
            send_mail(subject, message, from_email, [to_email])
            messages.success(self.request, "thanks for your contact!")
        except Exception as e:
            logger.error("failed send_mail to %s", from_email)
            logger.error("because of %s", e)
            messages.error(self.request, "sorry! failed submit because of server gone!")
        return responce
