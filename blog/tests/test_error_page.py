from unittest import mock

from django.test import TestCase
from django.core.exceptions import SuspiciousOperation, PermissionDenied
from django.http import Http404
from django.test.utils import override_settings

from ..models import Article


class BaseTestCases:
    class ErrorViewTestCase(TestCase):
        """
        - 以下のエラーで目的のメッセージが使用されているか、
          ステータスコードが目的のものか確認
            - 404
            - 403
            - 400
        """

        def setUp(self):
            """
            status_code : int
                目的のステータスコード
            msg : str
                目的のメッセージ
            exception : type
                目的のステータスコードを出すためのエラー
            """
            self.article = Article.objects.create(title="error test", content="", is_public=True)

        def raise_(self, *args, **kwargs):
            raise self.exception

        def test_status_code(self):
            with mock.patch("blog.views.ArticleDetailView.get", self.raise_):
                res = self.client.get(self.article.get_absolute_url())
                self.assertEqual(res.status_code, self.status_code)
                self.assertContains(res, self.msg, status_code=res.status_code)


class Error400ViewTestCase(BaseTestCases.ErrorViewTestCase):
    def setUp(self):
        self.status_code = 400
        self.msg = "リクエストに不整合がありました"
        self.exception = SuspiciousOperation
        super().setUp()

class Error403ViewTestCase(BaseTestCases.ErrorViewTestCase):
    def setUp(self):
        self.status_code = 403
        self.msg = "そのページは見ることができません"
        self.exception = PermissionDenied
        super().setUp()

class Error404ViewTestCase(BaseTestCases.ErrorViewTestCase):
    def setUp(self):
        self.status_code = 404
        self.msg = "ページが見つかりませんでした"
        self.exception = Http404
        super().setUp()
