from urllib.parse import quote
from django.test import TestCase
from django.urls import reverse

from ..models import Article


fixtures_file = "fixture.json"


class ArticleModelTestCase(TestCase):
    """
    - 記事モデルの確認
        - モデルから正しいURLが取得できるか確認
        - プロパティ(次作成日時のインスタンス取得)の確認
        - プロパティ(前作成日時のインスタンス取得)の確認
    """
    fixtures = [fixtures_file]

    def setUp(self):
        self.latest = Article.objects.filter(is_public=True).order_by("created_at").last()
        self.oldest = Article.objects.filter(is_public=True).order_by("created_at").first()
        self.article = Article.objects.filter(is_public=True)\
                                      .exclude(title=self.latest)\
                                      .exclude(title=self.oldest)\
                                      .order_by("?")\
                                      .first()
        self.url = self.article.get_absolute_url()
        self.res = self.client.get(self.url)

    def test_url(self):
        url = self.article.get_absolute_url()
        self.assertEqual(quote(url), reverse("blog:detail", kwargs={"title": self.article.title}))

    def test_display_previous_link_in_latest_article(self):
        _prev = Article.objects.filter(created_at__lt=self.article.created_at, is_public=True)\
                               .order_by("created_at")\
                               .last()
        self.assertEqual(self.article.get_prev_article, _prev)

    def test_display_next_link_in_oldest_article(self):
        _next = Article.objects.filter(created_at__gt=self.article.created_at, is_public=True)\
                               .order_by("created_at")\
                               .first()
        self.assertEqual(self.article.get_next_article, _next)
