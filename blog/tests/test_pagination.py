from django.test import TestCase

from ..models import Article


fixtures_file = "fixture.json"


class BaseTestCases:
    class ArticlePaginationTestCase(TestCase):
        """
        - ページネーション機能
            - ページ分割の確認
            - ページ移動の確認
            - 存在しないページへの移動失敗の確認
            - 表示していないページが表示されていないことの確認
        """

        def setUp(self):
            """
            以下は継承先で定義

            article_number : int
                総記事数
            pagenated_by : int
                ページネーションする記事の単位数
            page_num : int
                初期ページナンバー
            move_page_num : int
                移動先のページナンバー
            non_display_page : int
                移動先のページのページネーションに表示されないページナンバー
            """
            # 記事作成
            Article.objects.bulk_create([
                Article(
                    title=str(i),
                    content=str(i),
                    is_public=True
                ) for i in range(self.article_number)
            ])
            self.url = self.get_url(self.page_num)
            self.next_url = self.get_url(self.move_page_num)
            self.res = self.client.get(self.url)

        def get_url(self, page_num):
            return "/articles/?page={}".format(page_num)

        def test_devide_pages(self):
            _from = self.pagenated_by * (self.page_num - 1)
            _to = self.pagenated_by * self.page_num
            articles = Article.objects.filter(is_public=True).order_by("-created_at")[_from:_to]
            self.assertQuerysetEqual(self.res.context["articles"], map(repr, articles))

        def test_move_page(self):
            res = self.client.get(self.next_url)
            self.assertEqual(res.status_code, 200)

        def test_move_not_exist_page(self):
            not_exist_page = 1000
            res = self.client.get(self.get_url(not_exist_page))
            self.assertEqual(res.status_code, 404)

        def test_non_display_page(self):
            if hasattr(self, "non_display_page"):
                self.assertContains(self.res, self.non_display_page)


class ArticlePaginationTestCaseLessThan5(BaseTestCases.ArticlePaginationTestCase):
    def setUp(self):
        self.article_number = 40
        self.pagenated_by = 10
        self.page_num = 1
        self.move_page_num = 4
        super().setUp()


class ArticlePaginationTestCaseEqual5(BaseTestCases.ArticlePaginationTestCase):
    def setUp(self):
        self.article_number = 50
        self.pagenated_by = 10
        self.page_num = 1
        self.move_page_num = 5
        super().setUp()


class ArticlePaginationTestCaseGreaterThan5(BaseTestCases.ArticlePaginationTestCase):
    def setUp(self):
        self.article_number = 100
        self.pagenated_by = 10
        self.page_num = 1
        self.move_page_num = 5
        self.non_display_page = 2
        super().setUp()
