from django.test import TestCase


fixtures_file = "fixture.json"


class SEOTestCase(TestCase):
    """
    - SEO対策テストケース
        - sitemapが取得できることの確認
        - robots.txtが取得できることの確認
    """

    fixtures = [fixtures_file]

    def setUp(self):
        pass

    def test_sitemap(self):
        url = "/sitemap.xml"
        res = self.client.get(url)
        self.assertEqual(res.status_code, 200)

    def test_robots(self):
        url = "/robots.txt"
        res = self.client.get(url)
        self.assertEqual(res.status_code, 200)
