from django.test import TestCase

from ..models import Article
from ..views import ArticleListView


fixtures_file = "fixture.json"


class ArticleListViewTestCase(TestCase):
    """
    - 記事一覧表示
        - states code 200の確認
        - ビュー関数の使用確認
        - タグ表示の確認
        - 非公開の記事が一覧にないことの確認
    """
    fixtures = [fixtures_file]

    def setUp(self):
        # 記事一覧にあるarticleからtagを持つarticleをインスタンス属性として用意する
        self.res = self.client.get("/articles/")
        articles = self.res.context["articles"]
        for article in articles:
            if article.tags.all():
                self.article = article
                break

    def test_status_code_200(self):
        self.assertEqual(self.res.status_code, 200)

    def test_resolve_url_of_list(self):
        self.assertEqual(self.res.resolver_match.func.view_class, ArticleListView)

    def test_listed_tag(self):
        tags = self.article.tags.all()
        for tag in tags:
            self.assertContains(self.res, tag)

    def test_non_public_article(self):
        articles = self.res.context["articles"]
        for article in articles:
            self.assertEqual(article.is_public, True)
