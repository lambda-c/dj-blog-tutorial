from django.test import TestCase
from django.core import mail
from django.test.utils import override_settings

from ..models import Contact
from ..views import ContactCreateView


class ContactViewTestCase(TestCase):
    """
    - 連絡画面表示
        - states code 200の確認
        - ビュー関数の使用確認
    """
    def setUp(self):
        self.url = "/contact/"
        self.res = self.client.get(self.url)

    def test_status_code_200(self):
        self.assertEqual(self.res.status_code, 200)

    def test_resolve_url_of_list(self):
        self.assertEqual(self.res.resolver_match.func.view_class, ContactCreateView)


class ContactTestCase(TestCase):
    """
    - 連絡の投稿
        - 連絡投稿のDB保存確認
        - 連絡投稿のログ表示確認
        - 連絡投稿のメール送信失敗のエラー確認
        - 連絡投稿後のリダイレクト
        - 不正な連絡投稿の失敗確認
        - 投稿後のメッセージ表示確認
        - CSRFトークンの確認
    """
    def setUp(self):
        self.url = "/contact/"
        self.data = {
            "name": "test",
            "subject": "this is test",
            "mail": "test@localhost.localdomain",
            "description": "this contact is test."
        }
        self.res = self.client.post(self.url, self.data)

    def test_post_save_database(self):
        contact = Contact.objects.get(**self.data)
        for key in self.data:
            self.assertEqual(self.data[key], getattr(contact, key))

    def test_post_email_box(self):
        data = self.data.copy()
        data["name"] = "log test"
        res = self.client.post(self.url, data)
        self.assertGreaterEqual(len(mail.outbox), 1)

    @override_settings(EMAIL_BACKEND="")
    def test_post_exception(self):
        with self.assertLogs('blog', level="DEBUG") as log:
            res = self.client.post(self.url, self.data, follow=True)
        self.assertIn('ERROR:blog.views:failed send_mail to {}'.format(self.data["mail"]), log.output)
        self.assertContains(res, "sorry")

    def test_post_redirect(self):
        self.assertRedirects(self.res, self.url)

    def test_post_invalid_comment(self):
        """
        本当は分けるべきだが、面倒なので以下をまとめて実施する

        - 空データPOST
        - 許容範囲外の長さを持つデータPOST
        """
        # name部分が空データ、データがinvalidなので自画面の200を得る
        data = self.data.copy()
        data["name"] = ""
        res = self.client.post(self.url, data)
        self.assertEqual(res.status_code, 200)

        # 長さが3000byte、データがinvalidなので自画面の200を得る
        data = self.data.copy()
        data["name"] = "test"*1000
        res = self.client.post(self.url, data)
        self.assertEqual(res.status_code, 200)

    def test_display_message(self):
        res = self.client.post(self.url, self.data, follow=True)
        self.assertContains(res, "thanks for")

    def test_exist_csrf_token(self):
        res = self.client.get(self.url)
        self.assertContains(res, 'csrfmiddlewaretoken')
