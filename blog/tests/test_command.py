from io import StringIO

from django.test import TestCase
from django.core.management import call_command

from ..models import Article


fixtures_file = "fixture.json"


class CommandTestCase(TestCase):
    """
    - カスタムコマンドのテストケース
        - キャッシュクリア後に正しい最新ページが表示されるか確認
    """
    fixtures = [fixtures_file]

    def setUp(self):
        # 最新ページのキャッシュ作成
        self.url = Article.objects.filter(is_public=True).first().get_absolute_url()
        self.client.get(self.url)

        # 最新ページ作成
        article = Article.objects.create(title="cache test", content="test", is_public=True)

    def test_show_latest_page_after_cacheclear(self):
        # キャッシュのため、作成した最新ページがないことを確認
        res = self.client.get(self.url)
        self.assertNotContains(res, "cache test")

        # キャッシュクリア
        with StringIO() as out:
            call_command("clearcache", stdout=out)
            self.assertIn("cleared cache", out.getvalue())

        # キャッシュクリア後に作成した最新ページがあることを確認
        res = self.client.get(self.url)
        self.assertContains(res, "cache test")
