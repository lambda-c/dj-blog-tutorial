from urllib.parse import quote
from django.test import TestCase
from django.urls import reverse
from django.http import QueryDict
from django.core.cache import cache

from ..models import Article, Tag
from ..views import ArticleDetailView
from ..forms import CommentForm


fixtures_file = "fixture.json"


class ArticleDetailViewTestCase(TestCase):
    """
    - 記事詳細表示
        - states code 200の確認
        - ビュー関数の使用確認
        - タグ表示の確認
        - 存在しない記事表示におけるstates code 404の確認
        - 非公開の記事の記事表示におけるstates code 404の確認
        - 参照前後でカウンターがインクリメントされているか
    """
    fixtures = [fixtures_file]

    def setUp(self):
        # tagを持つ任意の公開articleをインスタンス属性として用意する
        # そのarticleに対して記事詳細のテストを行う
        for article in Article.objects.filter(is_public=True).order_by('?'):
            if article.tags.all():
                self.article = article
                break
        self.res = self.client.get(self.article.get_absolute_url())

    def test_status_code_200(self):
        self.assertEqual(self.res.status_code, 200)

    def test_resolve_url_of_list(self):
        self.assertEqual(self.res.resolver_match.func.view_class, ArticleDetailView)

    def test_listed_tag(self):
        tags = self.article.tags.all()
        for tag in tags:
            self.assertContains(self.res, tag)

    def test_status_code_404(self):
        res = self.client.get("/articles/そんな記事は存在しない")
        self.assertEqual(res.status_code, 404)

    def test_status_code_404_non_public_article(self):
        non_public_article = Article.objects.filter(is_public=False).first()
        res = self.client.get(non_public_article.get_absolute_url())
        self.assertEqual(res.status_code, 404)

    def test_counter_increment(self):
        article = Article.objects.filter(is_public=True)\
                                 .order_by("?")\
                                 .exclude(title=self.article)\
                                 .first()
        now_view_count = article.counter
        for _ in range(10):
            self.client.get(article.get_absolute_url())
        self.assertEqual(now_view_count+10, Article.objects.get(title=article).counter)


class CommentTestCase(TestCase):
    """
    - 記事へのコメント表示、投稿
        - コメントフォームの確認
        - コメント表示の確認
        - コメント投稿の確認
        - コメント投稿後リダイレクトの確認
        - 不正なコメント投稿の失敗確認
        - 非公開記事へのコメント投稿の失敗確認
        - CSRFトークンの確認
    """
    fixtures = [fixtures_file]

    def setUp(self):
        self.article = Article.objects.filter(is_public=True).order_by('?').first()
        self.url = self.article.get_absolute_url()
        self.res = self.client.get(self.url)

    def post_comment(self):
        data = {"name": "テスト", "description": "これはテスト投稿です"}
        res = self.client.post(self.url, data)
        return res

    def test_exist_comment_form(self):
        form = self.res.context['form']
        self.assertIsInstance(form, CommentForm)

    def test_display_comment(self):
        for comment in self.article.comments.all():
            self.assertContains(self.res, comment)

    def test_post_comment_redirect(self):
        res = self.post_comment()
        self.assertRedirects(res, quote(self.url))
        self.assertEqual(res.status_code, 302)

    def test_posted_comment(self):
        self.post_comment()
        res = self.client.get(self.url)
        self.assertContains(res, "これはテスト投稿です")

    def test_post_invalid_comment(self):
        """
        本当は分けるべきだが、面倒なので以下をまとめて実施する

        - 空データPOST
        - 許容範囲外の長さを持つデータPOST
        """
        # name部分が空データ、データがinvalidなので自画面の200を得る
        data = {"name": "", "description": "これはテスト投稿ですよ"}
        res = self.client.post(self.url, data)
        self.assertEqual(res.status_code, 200)

        # 長さが3000byte、データがinvalidなので自画面の200を得る
        data = {"name": "テスト"*1000, "description": "これはテスト投稿ですよ"}
        res = self.client.post(self.url, data)
        self.assertEqual(res.status_code, 200)

    def test_post_comment_to_non_public_article(self):
        data = {"name": "テスト", "description": "これはテスト投稿です"}
        non_public_article = Article.objects.filter(is_public=False).first()
        res = self.client.post(non_public_article.get_absolute_url(), data)
        self.assertEqual(res.status_code, 405)

    def test_exist_csrf_token(self):
        self.assertContains(self.res, 'csrfmiddlewaretoken')


class ArticleLinkTestCase(TestCase):
    """
    - 関係ページリンク
        - 最新ページリンクの表示確認
        - 人気ページリンクの表示確認
        - 最新記事での次ページリンクが存在しないことの確認
        - 最新記事での前ページリンクが存在することの確認
        - 最古記事での次ページリンクが存在することの確認
        - 最古記事での前ページリンクが存在しないことの確認
        - 類似ページリンクの表示確認
    """

    fixtures = [fixtures_file]

    def setUp(self):
        self.latest = Article.objects.filter(is_public=True).order_by("created_at").last()
        self.oldest = Article.objects.filter(is_public=True).order_by("created_at").first()
        self.article = Article.objects.filter(is_public=True)\
                                      .exclude(title=self.latest)\
                                      .exclude(title=self.oldest)\
                                      .order_by("?")\
                                      .first()
        self.res = self.client.get(self.article.get_absolute_url())

    def test_display_latest_link(self):
        self.assertContains(self.res, self.latest.get_absolute_url())

    def test_display_popular_link(self):
        # 人気ページ5個作成
        populars = Article.objects.filter(is_public=True).order_by('?')[:5]
        for index, article in enumerate(populars):
            article.counter = 10 + index
            article.save()

        cache.clear()
        res = self.client.get(self.article.get_absolute_url())
        for popular in populars:
            self.assertContains(res, popular.get_absolute_url())

    def test_not_display_next_link_in_latest_article(self):
        next_sign = "&gt;"      # means >
        res = self.client.get(self.latest.get_absolute_url())
        self.assertNotContains(res, next_sign)

    def test_display_previous_link_in_latest_article(self):
        # failure
        _prev = self.latest.get_prev_article
        res = self.client.get(self.latest.get_absolute_url())
        self.assertContains(res, _prev.get_absolute_url())

    def test_display_next_link_in_oldest_article(self):
        # failure
        _next = self.oldest.get_next_article
        res = self.client.get(self.oldest.get_absolute_url())
        self.assertContains(res, _next.get_absolute_url())

    def test_not_display_previous_link_in_oldest_article(self):
        prev_sign = "&lt;"      # means <
        res = self.client.get(self.oldest.get_absolute_url())
        self.assertNotContains(res, prev_sign)

    def test_display_similar_article_link(self):
        # 類似ページ作成
        tag = Tag.objects.create(name="___test")
        article = Article.objects.create(is_public=True, title="類似ページリンクテスト", content="")
        similar = Article.objects.filter(is_public=True).order_by('?').first()
        article.tags.set([tag])
        similar.tags.set([tag])
        res = self.client.get(article.get_absolute_url())
        self.assertContains(res, similar.get_absolute_url())


class CommentNestTestCase(TestCase):
    """
    - コメント返信のテストケース
        - コメントツリーの入れ子構造の確認
        - コメント入れ子最大数の確認
        - 存在しないコメントへの返信失敗の確認
    """
    def setUp(self):
        data = dict(title="test nest", content="test description", is_public=True)
        article = Article(**data).save()
        self.url = Article.objects.first().get_absolute_url()
        # コメントの入れ子構造は以下のようにする
        # 1 3 6 7 8
        #   4
        # 2 5
        nest = (
            # parent, id, depth
            (None, 1, 0), 
            (None, 2, 0), 
            (1, 3, 1), 
            (1, 4, 1),
            (2, 5, 1),
            (3, 6, 2), 
            (6, 7, 3), 
            (7, 8, 4),
        )
        from ..models import Comment
        for parent, _id, depth in nest:
            Comment.objects.create(
                article=Article.objects.first(),
                parent_id=parent,
                id=_id,
                depth=depth,
                name="name "+str(_id),
                description= "description "+str(_id),
            )

    def get_comment(self, parent, _id):
        d = {} if parent is None else {"parent": parent}
        d.update({
            "name": "name "+str(_id),
            "description": "description "+str(_id),
        })
        return d

    def test_nest_comment_tree(self):
        from bs4 import BeautifulSoup as bs
        res = self.client.get(self.url)
        soup = bs(res.content.decode('utf-8'), features="html.parser")
        comment1 = soup.find(id="comment_1")
        comment2 = soup.find(id="comment_2")
        comment3 = soup.find(id="comment_3")
        self.assertTrue(comment1.find(id="comment_3"))
        self.assertTrue(comment1.find(id="comment_4"))
        self.assertTrue(comment2.find(id="comment_5"))
        self.assertTrue(comment3.find(id="comment_6"))

    def test_fail_post_nest_over(self):
        res = self.client.post(self.url, self.get_comment(8, 9), follow=True)
        query = QueryDict("name=test&description=test desciption&parent=8")
        self.assertContains(res, "これ以上、返信できません")
        self.assertEqual(res.status_code, 200)
        self.assertFalse(CommentForm(query).is_valid())

    def test_fail_responce_to_not_exist_comment(self):
        res = self.client.post(self.url, self.get_comment(10000, 9), follow=True)
        query = QueryDict("name=test&description=test desciption&parent=10000")
        self.assertEqual(res.status_code, 200)
        self.assertFalse(CommentForm(query).is_valid())
