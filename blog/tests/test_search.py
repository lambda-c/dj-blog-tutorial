from django.test import TestCase

from ..models import Article, Tag


class SearchTestCase(TestCase):
    """
    - 検索のテストケース
        - 検索で得られる`queryset`が正しいものか確認
          （検索キーワードは記事タイトル、記事内容、タグでテストする）
        - 検索キーワードが空で検索した時、普通の記事一覧画面が表示されるか確認
        - DBにない文字列で検索して、検索結果が0であることの確認
    """

    def setUp(self):
        self.url = "/articles/"
        for i in range(10):
            data = {
                "title": "title"+str(i)*5,
                "content": "content"+str(i)*5,
                "is_public": True
            }
            article = Article.objects.create(**data)
            tag = Tag.objects.create(name="tag"+str(i)*5)
            article.tags.set([tag])
        self.articles = Article.objects.order_by("-created_at")

    def search_queryset_by(self, q, cond):
        # 補助関数
        res = self.client.get(self.url, data={"q": q})
        articles = Article.objects.filter(**cond)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(len(res.context["articles"]), 1)
        self.assertQuerysetEqual(res.context["articles"], map(repr, articles))

    def test_search_queryset_by_title(self):
        self.search_queryset_by(q="title111", cond={"title": "title11111"})

    def test_search_queryset_by_content(self):
        self.search_queryset_by(q="content222", cond={"content": "content22222"})

    def test_search_queryset_by_tag(self):
        tag = Tag.objects.get(name="tag33333")
        self.search_queryset_by(q="tag333", cond={"tags": tag})

    def default_article_list_view(self, res):
        # 補助関数
        self.assertEqual(res.status_code, 200)
        self.assertEqual(len(res.context["articles"]), 10)
        self.assertQuerysetEqual(res.context["articles"], map(repr, self.articles))

    def test_empty_search(self):
        res = self.client.get(self.url, data={"q": ""})
        self.default_article_list_view(res)

    def test_not_exist_wrod_search_result(self):
        res = self.client.get(self.url, data={"q": "1234567890"})
        self.default_article_list_view(res)
        self.assertContains(res, "1234567890"+"の検索結果は0件でした")
